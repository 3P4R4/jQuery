$(document).ready(function () {
    $("#editarE").addClass("d-none");
    // Alert para mostrar los empleados
    $('#empleados').click(function (e) { 
        e.preventDefault();
        $('#alert').html('');
        $.getJSON("http://localhost/ajax/empleados.php", {"accion":"leer"}, function (data) {
                $.each(data, function (index, valor) { 
                     $('#alert').html($('#alert').html()+`
                        <li style="font-size:24px; text-transform: capitalize;">
                        <svg class="pencil" width="24" height="24" viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg"
                        data-id="${valor.id}"
                        data-nombre="${valor.nombre}"
                        data-puesto="${valor.puesto}"
                        data-edad="${valor.edad}"
                        ><path d="M 229 615C 229 615 382 766 382 766C 382 766 356 792 356 792C 351 798 344 800 338 802C 338 802 210 828 210 828C 186 832 163 810 169 785C 169 785 195 659 195 659C 195 652 199 646 203 641C 203 641 229 615 229 615M 713 137C 713 137 865 288 865 288C 865 288 432 716 432 716C 432 716 279 565 279 565C 279 565 713 137 713 137M 839 25C 848 25 858 29 865 36C 865 36 967 137 967 137C 980 150 980 173 967 187C 967 187 915 237 915 237C 915 237 763 86 763 86C 763 86 815 36 815 36C 821 29 830 25 839 25C 839 25 839 25 839 25M 150 13C 150 13 650 13 650 13C 664 12 676 19 683 31C 690 43 690 57 683 69C 676 81 664 88 650 88C 650 88 150 88 150 88C 138 88 121 95 108 108C 95 121 88 138 88 150C 88 150 88 850 88 850C 88 862 95 879 108 892C 121 905 138 912 150 912C 150 912 850 912 850 912C 862 912 879 905 892 892C 905 879 912 862 912 850C 912 850 912 350 912 350C 912 336 919 324 931 317C 943 310 957 310 969 317C 981 324 988 336 987 350C 987 350 987 850 987 850C 987 887 970 920 945 945C 921 970 888 987 850 987C 850 987 150 987 150 987C 113 987 79 970 55 945C 30 921 13 887 13 850C 13 850 13 150 13 150C 13 113 30 79 55 55C 79 30 113 13 150 13C 150 13 150 13 150 13"/></svg>
                        
                        <svg class="borrar" width="24" height="24" viewBox="0 0 1000 1000" xmlns="http://www.w3.org/2000/svg"
                        data-id="${valor.id}"
                        ><path d="M 575 63C 609 63 638 91 638 125C 638 125 638 187 638 187C 638 187 849 187 849 187C 870 187 887 204 887 225C 887 245 870 262 849 262C 849 262 151 263 151 263C 130 263 113 246 113 225C 113 205 130 188 151 188C 151 188 363 188 363 188C 363 188 363 125 363 125C 363 125 362 125 362 125C 362 91 391 63 425 63C 425 63 575 63 575 63M 438 187C 438 187 563 187 563 187C 563 187 563 138 563 138C 563 138 438 138 438 138C 438 138 438 187 438 187M 780 317C 780 317 738 854 738 854C 738 856 738 859 737 861C 733 878 725 896 711 911C 697 926 676 938 652 939C 652 939 651 939 651 939C 651 939 351 939 351 939C 351 939 350 939 350 939C 325 938 303 926 289 911C 275 895 267 877 264 857C 264 856 264 856 264 855C 264 855 220 316 220 316C 220 316 780 317 780 317"/></svg>

                        ${valor.nombre} -- ${valor.puesto} -- ${valor.edad}
                        

                        </li>
                     `);
                });
            }
        );
    });
    // Pesataña Crear
    $("#crearE").click(function (e) { 
        e.preventDefault();
        
        let nombre=$("#nombre").val();
        let puesto=$("#puesto").val();
        let edad=$("#edad").val();
        let obj={"accion":"insertar","nombre":nombre,"puesto":puesto,"edad":edad};
        $.post("http://localhost/ajax/empleados.php", obj,
            function (data) {
                if(data==="1"){
                    $("#nombre").val('');
                    $("#puesto").val('');
                    $("#edad").val('');
                    
                }
            },
        
        );
        
    });
    // Agregando el icono para editar los registros
    $(document).on("click", ".pencil", function(){
        $("#crear").click();
        $("#crear").html("Editar");
        $("#crearE").addClass("d-none");
        $("#editarE").removeClass("d-none");
        //Agregare aqui los valores que estan en el bloque del ALERT DE EMPLEADOS con el fin de que al darle click al ICONO DE EDITAR este enseguida me cargue automaticamente los campos correspondiente a cada INPUT y estos valores son los de DATA-ID y DATA-ETC,ETC...
        // AL id DEL CAMPO nombre
        $("#nombre").val($(this).data("nombre"));
        $("#puesto").val($(this).data("puesto"));
        $("#edad").val($(this).data("edad"));
        $("#id").val($(this).data("id"));
           
    });
    // Agregando el icono de basura para eliminar elementos del registro
    $(document).on("click", ".borrar", function(){
      let id=$(this).data("id");
      let obj={"accion":"borrar", "id":id};
      $.post("http://localhost/ajax/empleados.php", obj,  
      function (data) {
                console.log(data);  
               if (data=="1") {
                  $("#empleados").click();
              }
          }
      );
      
    });
    

    // Al cambia por X causa a la pestaña de LISTAR este desvuelva los valor a la pestaña CREAR
    $("#listar").click(function (e) { 
        e.preventDefault();
        $("#crear").html("Crear");
        $("#crearE").removeClass("d-none");
        $("#editarE").addClass("d-none");
        
    });
    // Al darle click al boton EDITAR este debe llevarnos de nuevo al tab  LISTAR y forzar un click al boton LISTAR EMPLEADOS, pero antes de ello debe verificar que si la accón es correcta, es decir si entrega VALOR 1 en el backend este es valido y realiza la funcion de llevarnos al tab LISTAR, en su defecto saldra un ALERT con el texto ERROR o el que queramos colocarle.
    //L a ACCION cambia a vaor EDITAR para que en la base de datos podamos hacer un UPDATE.
    $("#editarE").click(function (e) { 
        e.preventDefault();
        let nombre=$("#nombre").val();
        let puesto=$("#puesto").val();
        let edad=$("#edad").val();
        let id=$("#id").val();
        let obj={"accion":"editar","nombre":nombre,"puesto":puesto,"edad":edad, "id": id};
        $.post("http://localhost/ajax/empleados.php", obj,
            function (data) {
                if(data==="1"){
                    $("#nombre").val('');
                    $("#puesto").val('');
                    $("#edad").val('');
                    $("#id").val('');
                    $("#listar-tab").click();
                    $("#empleados").click();
                    $("#crear").html("Crear");
                    $("#crearE").removeClass("d-none");
                    $("#editarE").addClass("d-none");
                }
            },
        
        );
    });
    
});